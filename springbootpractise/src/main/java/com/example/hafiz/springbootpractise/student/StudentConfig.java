package com.example.hafiz.springbootpractise.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static java.time.Month.*;

@Configuration
public class StudentConfig {
    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student hafiz = new Student(
                    "Hafiz",
                    "hafiz.rahim@gmail.com",
                    LocalDate.of(1964, NOVEMBER, 23)
            );

            Student farisya = new Student(
                    "Farisya",
                    "sya.azahar@gmail.com",
                    LocalDate.of(1987, JUNE, 23)
            );

            repository.saveAll(
               List.of(hafiz, farisya)
            );

        };
    }
}
